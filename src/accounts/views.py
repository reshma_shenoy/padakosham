from django.shortcuts import render
from django.urls import reverse


# Create your views here.
from registration.backends.simple.views import RegistrationView

class registration_redirect(RegistrationView):
    def get_success_url(self, request):
        return "/home/"
