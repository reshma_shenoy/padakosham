from __future__ import unicode_literals

from django.apps import AppConfig


class UiTestConfig(AppConfig):
    name = 'ui_test'
