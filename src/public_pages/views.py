from django.shortcuts import render, redirect

def index(request):
    if request.user.is_authenticated:
        return redirect('/home/')
    return render(request, "apps/public_pages/index.html")

