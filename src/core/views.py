from django.views import View
from django.views.generic import FormView, TemplateView
from django.http import HttpResponse
from django.shortcuts import render, redirect

from .forms import WordAddForm, StudentAddForm, WordMeaningAddForm
from .models import Word

class HomeView(TemplateView):
    template_name = 'apps/UI_test/home.html'

class WordsReviewView(View):
    def get(self, request):
        unverified_words = Word.objects.filter(worddetails__is_verified=False)
        context = {
            'unverified_words': unverified_words
        }
        return render(request, "apps/core/review.html", context=context)

class SuccessView(View):
    def get(self, request):
        unverified_words = Word.objects.filter(worddetails__is_verified=False)
        context = {
            'unverified_words': unverified_words
        }
        return render(request, "apps/core/result.html", context=context)

class WordsEditView(View):
    def get(self, request, pk):
        return HttpResponse("LOL!")


class WordsApproveView(View):
    def post(self, request, pk):
        word = Word.objects.get(id=pk)
        word.worddetails.is_verified = True
        word.worddetails.verified_by = request.user
        word.worddetails.save()
        return redirect('review')


class StudentsAddView(FormView):
    template_name = 'apps/core/students_add.html'
    form_class = StudentAddForm

    def post(self, *args, **kwargs):
        return redirect('/home/words/add/')


class WordsAddView(FormView):
    template_name = 'apps/core/words_add.html'
    form_class = WordAddForm

    def form_valid(self, form):
        add_another = self.request.POST.get('add_another')
        finish = self.request.POST.get('finish')

        if finish:
            return redirect('/home/words/success/')
        else:
            return redirect('/home/words/add/')

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['form_meaning'] = WordMeaningAddForm
        return context
