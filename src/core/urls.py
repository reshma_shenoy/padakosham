from django.conf.urls import url

from .views import (
    WordsAddView, WordsReviewView, WordsEditView, WordsApproveView,
    StudentsAddView,SuccessView, HomeView
)


app_name = 'core'

urlpatterns = [
    url(r'^review/', WordsReviewView.as_view(), name='words_review'),
    url(r'^words/success', SuccessView.as_view(), name='words_success'),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^words/add/$', WordsAddView.as_view(), name='words_add'),
    url(r'^words/(?P<pk>[0-9]+)/edit/$', WordsEditView.as_view(),
        name='words_edit'),
    url(r'^words/(?P<pk>[0-9]+)/approve/$', WordsApproveView.as_view(),
        name='words_approve'),
]

urlpatterns += [
    url(r'^students/add/$', StudentsAddView.as_view(), name='students_add'),
]
